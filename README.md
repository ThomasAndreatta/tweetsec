# TWEETSEC
## Purpose
Due to the fact that following an # is not allowed and i'm not interested in all the other useless tweet that people makes
this bot will retweet the most liked tweet in the last <tot> minutes with certain # making easier the "stay up to date" process with certain selected arguments.

## File Content

* docker/:\
This folder contains all the docker file. They're composed as follow:
    * create_image.sh:\
    Build an image with all the requirements for the bot, file included. A new directory called logs will be created and shared with the docker container, all the bot logs will be stored there.
    * Dockerfile:\
    Docker configuration file for the image building process.
    * run_docker:\
    Run the instance - if already present - of tweetsec bot in background, once the container stop it'll be automatically removed.
    * docker.sh:\
    Call create_image.sh and run_docker.sh sequentially. Create the image and run it.

* tweetsec.py:\
Bot himself.


## Working on
* telegram bot for updating the query and the timeing
-----


##### NOTE
* https://realpython.com/twitter-bot-python-tweepy/
* https://docs.tweepy.org/en/stable/api.html
