#!/bin/bash
#create docker image`
rm -rf ./app
mkdir ./app
cp ../tags.log ./app/
cp ../tweetsec.py ./app/
cp ../.env ./app/
touch ./app/tweeted.log
docker image rm tweetsec:latest
docker build --force-rm -f Dockerfile -t tweetsec .
