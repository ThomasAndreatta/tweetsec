#!/usr/bin/python3

from dotenv import load_dotenv
import os
import tweepy
import time
import datetime
from lazyt import Loader
# Use the variable with:

load_dotenv()

CONSUMER_KEY=os.getenv("CONSUMER_KEY")
CONSUMER_SECRET=os.getenv("CONSUMER_SECRET")
ACCESS_TOKEN=os.getenv("ACCESS_TOKEN")
ACCESS_SECRET=os.getenv("ACCESS_SECRET")
auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
api = tweepy.API(auth)

tweeted=[]
debug = False

def load_tweet():
    tweet=[]
    with open('tweeted.log','r') as f:
        tweet= f.read().split('\n')
    return [x for x in tweet if x]
tweeted=load_tweet()


def update_tweet(id):
    with open('tweeted.log','a') as f:
        f.write(str(id)+'\n')

def generate_query():
    #print("query")
    query=[]
    with open('tags.log','r') as f:
        lines=f.read().split('\n')
    lines=[x for x in lines if x]

    for line in lines:
        words = line.split(';')
        #print(words)
        qseparator = words[0].upper()

        tmp=''
        for word in words[1:]:
            #print(word,end=' ')
            tmp+=word+' '+qseparator+' '
        offset=len(qseparator)+2
        tmp=tmp[0:-offset]
        query.append(tmp)
    return query




def main():
    global tweeted

    pause = 45 * 60

    while True:
        #print('==========')
        query=generate_query()

        for qs in query:
            highest=-1
            id=''
            all_tweets = api.search(q=qs,result_type="popular", lang="en", count=15,tweet_mode="extended")
            tweets = [i for i in all_tweets if str(i.id) not in tweeted]
            #print("query: {}\nhow many: {}\n".format(qs,len(tweets)))
            for tweet in tweets:
                if(tweet.id in tweeted):

                    continue
                if(highest < tweet.favorite_count):
                    highest=tweet.favorite_count
                    id=tweet.id

            #print("got tweets")
            if(highest > -1 and debug == False):
                try:
                    update_tweet(id)
                    api.retweet(id)
                except Exception as e:
                    pass
                tweeted.append(str(id))

        #l = Loader("Sleeping...", "bonk", 0.05).start()
        #print("posted")
        time.sleep(pause)

        #l.stop()

def delete():


    timeline = tweepy.Cursor(api.user_timeline).items()
    deletion_count=0

    for tweet in timeline:
        # Where tweets are not in save list and older than cutoff date

        api.destroy_status(tweet.id)
        deletion_count += 1
        print(deletion_count)



if __name__ == '__main__':
    api.update_status('I\'m alive!\n{}'.format(datetime.datetime.now()))
    main()
    #delete()
